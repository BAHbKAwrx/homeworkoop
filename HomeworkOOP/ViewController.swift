//
//  ViewController.swift
//  HomeworkOOP
//
//  Created by Shmygovskii Ivan on 29.08.18.
//  Copyright © 2018 Shmygovskii Ivan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var cupImage: UIImageView!
    @IBOutlet weak var screenLabel: UILabel!
    
    var text = String()
    var sliderValue = 0
    let CoffeMachine666 = CoffeMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    }
    
    func drawWaterLine(_ currWater: Int) {
        let procent = (Double(currWater) / 1500) * 100
        let amount = Int(procent * 2.9)
        for i in 1..<amount + 1 {
            //let rect = CGRect.init(x: 291, y: 107, width: 16, height: 1) top level
            let rect = CGRect.init(x: 291, y: 400 - i, width: 16, height: 1)
            let box = UIView.init(frame: rect)
            box.backgroundColor = .cyan
            view.addSubview(box)
        }
    }
    
    func deleteWaterLine() {
        for i in 1..<293 {
            //let rect = CGRect.init(x: 291, y: 107, width: 16, height: 1) top level
            let rect = CGRect.init(x: 291, y: 400 - i, width: 16, height: 1)
            let box = UIView.init(frame: rect)
            box.backgroundColor = .white
            view.addSubview(box)
        }
    }

    @IBAction func addWaterButton() {
        text = "Current amount of water is: \(CoffeMachine666.addWater(sliderValue))"
        screenLabel.textColor = .blue
        screenLabel.text = text
        drawWaterLine(CoffeMachine666.waterAmount)
    }
    
    @IBAction func addMilkButton() {
        text = "Current amount of milk is: \(CoffeMachine666.addMilk(sliderValue))"
        screenLabel.textColor = .blue
        screenLabel.text = text
    }
    
    @IBAction func addBeansButton() {
        text = "Curr. amount of beans is: \(CoffeMachine666.addBeans(sliderValue))"
        screenLabel.textColor = .blue
        screenLabel.text = text
    }
    
    @IBAction func addWhiskeyButton() {
        text = "Curr. amount of whiskey is: \(CoffeMachine666.addWhiskey(sliderValue))"
        screenLabel.textColor = .blue
        screenLabel.text = text
    }
    
    @IBAction func americanoButton() {
        let image = UIImage.init(named: "Americano")
        text = CoffeMachine666.makeAmericano()
        if text == "Your americano is ready"{
            cupImage.image = image
            screenLabel.textColor = .blue
        }
        else {
            screenLabel.textColor = .red
        }
        screenLabel.text = text
        deleteWaterLine()
        drawWaterLine(CoffeMachine666.waterAmount)
    }
    
    @IBAction func irishButton() {
        let image = UIImage.init(named: "Irish")
        text = CoffeMachine666.makeIrish()
        if text == "Your irish is ready" {
            cupImage.image = image
            screenLabel.textColor = .blue
        }
        else {
            screenLabel.textColor = .red
        }
        screenLabel.text = text
    }
    
    @IBAction func cappucinoButton() {
        let image = UIImage.init(named: "Cappucino")
        text = CoffeMachine666.makeCappucino()
        if text == "Your cappucino is ready" {
            cupImage.image = image
            screenLabel.textColor = .blue
        }
        else {
            screenLabel.textColor = .red
        }
        screenLabel.text = text
    }
    
    @IBAction func clearButton() {
        text = "Current amount of waste is: \(CoffeMachine666.clearWastes())"
        screenLabel.textColor = .blue
        screenLabel.text = text
    }
    
    @IBAction func drinkButton() {
        let image = UIImage.init(named: "coffe1")
        text = "Welcome!"
        screenLabel.textColor = .blue
        screenLabel.text = text
        cupImage.image = image
    }
    
    @IBAction func changeSlider(_ sender: UISlider) {
        sliderLabel.text = String(Int(sender.value))
        sliderValue = Int(sender.value)
    }
    
}

