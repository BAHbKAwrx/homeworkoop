//
//  CoffeMachine.swift
//  HomeworkOOP
//
//  Created by Shmygovskii Ivan on 29.08.18.
//  Copyright © 2018 Shmygovskii Ivan. All rights reserved.
//

import UIKit

class CoffeMachine: NSObject {
    private let portionOfWater = 500 //ml
    private let portionOfMilk = 200
    private let portionOfBeans = 500 //g
    private let portionOfWhiskey = 150
    
    private let limitOfWater = 1500 //ml
    private let limitonOfMilk = 600
    private let limitOfBeans = 1300 //g
    private let limitOfWhiskey = 500
    private let limitOfWaste = 300
    
    var waterAmount = 0
    var milkAmount = 0
    var beansAmount = 0
    var whiskeyAmount = 0
    var wasteAmount = 0
    
    func addWater(_ portion: Int) -> Int {
        if waterAmount + portion <= limitOfWater {
            waterAmount += portion
        }
        else {
            waterAmount = limitOfWater
        }
        return waterAmount
    }
    
    func addMilk(_ portion: Int) -> Int {
        if milkAmount + portion <= limitonOfMilk {
            milkAmount += portion
        }
        else {
            milkAmount = limitonOfMilk
        }
        return milkAmount
    }
    
    func addBeans(_ portion: Int) -> Int {
        if beansAmount + portion <= limitOfBeans {
            beansAmount += portion
        }
        else {
            beansAmount = limitOfBeans
        }
        return beansAmount
    }
    
    func addWhiskey(_ portion: Int) -> Int {
        if whiskeyAmount + portion <= limitOfWhiskey {
            whiskeyAmount += portion
        }
        else {
            whiskeyAmount = limitOfWhiskey
        }
        return whiskeyAmount
    }
    
    func clearWastes() -> Int {
        wasteAmount = 0
        return wasteAmount
    }
    
    func makeCappucino() -> String {
        var result = String()
        if beansAmount - 150 < 0 {
            result = "Not enough beans"
        }
        if milkAmount - 100 < 0 {
            result = "Not enough milk"
        }
        if wasteAmount + 100 > limitOfWaste {
            result = "Too much waste"
        }
        if allConditionsReady(water: 0, milk: 100, beans: 150, whiskey: 0, waste: 100) {
            beansAmount -= 150
            milkAmount -= 100
            wasteAmount += 100
            result = "Your cappucino is ready"
        }
        return result
    }
    
    func makeAmericano() -> String {
        var result = String()
        if beansAmount - 100 < 0 {
            result = "Not enough beans"
        }
        if waterAmount - 200 < 0 {
            result = "Not enough water"
        }
        if wasteAmount + 100 > limitOfWaste {
            result = "Too much waste"
        }
        if allConditionsReady(water: 200, milk: 0, beans: 100, whiskey: 0, waste: 100) {
            beansAmount -= 100
            waterAmount -= 200
            wasteAmount += 100
            result = "Your americano is ready"
        }
        return result
    }
    
    func makeIrish() -> String {
        var result = String()
        if beansAmount - 150 < 0 {
            result = "Not enough beans"
        }
        if whiskeyAmount - 150 < 0 {
            result = "Not enough whiskey"
        }
        if wasteAmount + 100 > limitOfWaste {
            result = "Too much waste"
        }
        if allConditionsReady(water: 0, milk: 0, beans: 150, whiskey: 150, waste: 100) {
            beansAmount -= 150
            whiskeyAmount -= 150
            wasteAmount += 100
            result = "Your irish is ready"
        }
        return result
    }
    
    func allConditionsReady(water: Int, milk: Int, beans: Int, whiskey: Int, waste: Int) -> Bool {
        var result = false
        if (waterAmount - water >= 0) && (milkAmount - milk >= 0) && (beansAmount - beans >= 0) && (whiskeyAmount - whiskey >= 0) && (wasteAmount + waste <= limitOfWaste) {
            result = true
        }
        return result
    }

}
